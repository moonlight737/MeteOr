//
//  RealTimeWeather.swift
//  MeteOr
//
//  Created by Bachir Mounir BENMESBAH on 29/04/2023.
//

import UIKit

class RealTimeWeather: UIViewController {

    var ville: String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(ville)
        //barre_haut.topItem?.title = "Observation météo à \(ville)"
        recherche(ville: ville)

    }
    
    func recherche(ville: String)
    {
        do
        {
            let url = NSURL(string: "http://api.openweathermap.org/data/2.5/weather?appid=36137513a45ae35523bf4f1fcbaedbe4&units=metric&q=\(ville)")
            let texte:NSString = try NSString(contentsOf: url! as URL, usedEncoding: nil)
            let jsonData = texte.data(using: 4)
            let meteo: Meteo = try! JSONDecoder().decode(Meteo.self, from: jsonData!)
            
            print("La meteo de \(ville) a été chargé")
            
            print(meteo.wind.speed)
            
            
        } catch {
            print("Erreur de conversion")
        }

    }



}
