//
//  ViewController.swift
//  MeteOr
//
//  Created by Bachir Mounir BENMESBAH on 29/04/2023.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ville: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func rechercher(_ sender: UIButton) {
        

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "aller_vers_observation") {
            let observationVC : RealTimeWeather = segue.destination as! RealTimeWeather
            observationVC.ville = ville.text!
            //observationVC.meteo
        }
    }
}

